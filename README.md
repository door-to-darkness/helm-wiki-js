# Wiki.js
[Wiki.js](https://github.com/Requarks/wiki) is a modern, lightweight, and powerful wiki app.

## Requirements
- A `Secret` named `wiki-postgresql` with a key of `postgresql-password` set to a password
    - The Helm chart does not seem to reuse the `Secret` it creates to hold the password if `postgresql.existingSecret` is not provided. Rather, it regenerates and replaces it, meaning a Helm upgrade will fail. `postgresql.existingSecret` is set to ensure data is retained
    - The Helm chart does not seem to respect `postgresql.existingSecretKey` so the defaults are used for clarity
